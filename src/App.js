import {useEffect, useState} from "react";
import './App.css';

function App() {

  const [chessBoard, setChessBoard] = useState([])
  const [currentFigure, setCurrentFigure] = useState({x: 0, y: 0, name: ''})
  const [moveOrder, setMoveOrder] = useState('white');
  const [figureName, setFigureName] = useState('');
  // console.log(chessBoard);

  useEffect(() => {
    let result = [];
    let board = []

    // for (let i = 0; i < 8; i++) {
    //   const row = Array.from({length: 8});
    //   result.push(row);
    // }

    // for (let i = 0; i < 8; i++) {
    //   for (let j = 0; j < 8; j++) {
    //     board[i][j] = {x: 0, y: 0, name: ''}
    //   }
    // }

    function createAndFillTwoDArray({
                                      rows,
                                      columns,
                                      defaultValue
                                    }) {
      return Array.from({length: rows}, () => (
        Array.from({length: columns}, () => defaultValue)
      ))
    }

    result = createAndFillTwoDArray({
      rows: 8,
      columns: 8,
      defaultValue: {x: 0, y: 0, name: '', color: '', empty: true, background: ''}
    })

    // console.log(board)

    result[0][0] = {x: 0, y: 0, name: 'ладья', color: 'black', empty: false, background: ''}
    result[0][1] = {x: 0, y: 1, name: 'конь', color: 'black', empty: false, background: ''}
    result[0][2] = {x: 0, y: 2, name: 'слон', color: 'black', empty: false, background: ''}
    result[0][3] = {x: 0, y: 3, name: 'ферзь', color: 'black', empty: false, background: ''}
    result[0][4] = {x: 0, y: 4, name: 'король', color: 'black', empty: false, background: ''}
    result[0][5] = {x: 0, y: 5, name: 'слон', color: 'black', empty: false, background: ''}
    result[0][6] = {x: 0, y: 6, name: 'конь', color: 'black', empty: false, background: ''}
    result[0][7] = {x: 0, y: 7, name: 'ладья', color: 'black', empty: false, background: ''}

    result[1][0] = {x: 1, y: 0, name: 'пешка', color: 'black', empty: false, background: ''}
    result[1][1] = {x: 1, y: 1, name: 'пешка', color: 'black', empty: false, background: ''}
    result[1][2] = {x: 1, y: 2, name: 'пешка', color: 'black', empty: false, background: ''}
    result[1][3] = {x: 1, y: 3, name: 'пешка', color: 'black', empty: false, background: ''}
    result[1][4] = {x: 1, y: 4, name: 'пешка', color: 'black', empty: false, background: ''}
    result[1][5] = {x: 1, y: 5, name: 'пешка', color: 'black', empty: false, background: ''}
    result[1][6] = {x: 1, y: 6, name: 'пешка', color: 'black', empty: false, background: ''}
    result[1][7] = {x: 1, y: 7, name: 'пешка', color: 'black', empty: false, background: ''}

    result[6][0] = {x: 6, y: 0, name: 'пешка', color: 'white', empty: false, background: ''}
    result[6][1] = {x: 6, y: 1, name: 'пешка', color: 'white', empty: false, background: ''}
    result[6][2] = {x: 6, y: 2, name: 'пешка', color: 'white', empty: false, background: ''}
    result[6][3] = {x: 6, y: 3, name: 'пешка', color: 'white', empty: false, background: ''}
    result[6][4] = {x: 6, y: 4, name: 'пешка', color: 'white', empty: false, background: ''}
    result[6][5] = {x: 6, y: 5, name: 'пешка', color: 'white', empty: false, background: ''}
    result[6][6] = {x: 6, y: 6, name: 'пешка', color: 'white', empty: false, background: ''}
    result[6][7] = {x: 6, y: 7, name: 'пешка', color: 'white', empty: false, background: ''}

    result[7][0] = {x: 7, y: 0, name: 'ладья', color: 'white', empty: false, background: ''}
    result[7][1] = {x: 7, y: 1, name: 'конь', color: 'white', empty: false, background: ''}
    result[7][2] = {x: 7, y: 2, name: 'слон', color: 'white', empty: false, background: ''}
    result[7][3] = {x: 7, y: 3, name: 'ферзь', color: 'white', empty: false, background: ''}
    result[7][4] = {x: 7, y: 4, name: 'король', color: 'white', empty: false, background: ''}
    result[7][5] = {x: 7, y: 5, name: 'слон', color: 'white', empty: false, background: ''}
    result[7][6] = {x: 7, y: 6, name: 'конь', color: 'white', empty: false, background: ''}
    result[7][7] = {x: 7, y: 7, name: 'ладья', color: 'white', empty: false, background: ''}

    setChessBoard(result)
  }, []);

  // const checkCell = (x, y, name, color) => {
  //   let arr = [...chessBoard]
  //   if (name === 'пешка') {
  //     if (arr[x + 1][y].name && arr[x + 1][y].name) {
  //       console.log(arr[x][y].name)
  //     }
  //   }
  // }

  // const handleClickToFigure = (idx, name, color) => {
  //   let arr = chessBoard;
  //   let coords = idx.split(('_'))
  //   let x = Number(coords[0])
  //   let y = Number(coords[1])
  //   if (arr[x][y] && name !== '.') {
  //     setCurrentFigure({x, y, name})
  //   }
  // }

  const handleClickToFigure = (idx, name, color) => {
    let arr = chessBoard;
    let coords = idx.split(('_'))
    let x = Number(coords[0])
    let y = Number(coords[1])
    if (arr[x][y] && name !== '.') {
      setCurrentFigure({x, y, name})
    }
    setFigureName(name)
    if (moveOrder === 'white' && color === 'white') {  // ход белых
      // console.log('ход белых')
      // checkCell(x, y, name, color)
      if (name === 'пешка' && moveOrder === 'white') {
        if (x === 6) {
          arr[x - 1][y] = {name: '.', color, background: ''}
          arr[x - 2][y] = {name: '.', color, background: ''}
          setChessBoard(arr)
        } else {
          if (arr[x - 1][y]) {
            console.log(arr[x - 1][y])
            arr[x][y].background = '#d36a6a'
          } else {
            arr[x - 1][y] = {name: '.', color, background: ''}
            setChessBoard(arr)
          }
        }
      } else if (name === '.' && moveOrder === 'white') {
        if (x === 5) {
          arr[x + 1][y] = {x, y, name: '', color, background: ''}
          arr[x - 1][y] = {x, y, name: '', color, background: ''}
        } else if (x === 4) {
          arr[x + 1][y] = {x, y, name: '', color, background: ''}
          arr[x + 2][y] = {x, y, name: '', color, background: ''}
        } else {
          arr[x + 1][y] = {x, y, name: '', color, background: ''}
        }
        arr[x][y] = {x, y, name: 'пешка', color, background: '' }
        setChessBoard(arr)
        setMoveOrder('black')
      }
    } else if (color === 'black') {  // ход черных
      // console.log('ход черных')
      if (name === 'пешка' && moveOrder === 'black') {
        if (x === 1) {
          arr[x + 1][y] = {name: '.', color}
          arr[x + 2][y] = {name: '.', color}
          setChessBoard(arr)
        } else {
          if (arr[x - 1][y]) {
            console.log(arr[x - 1][y])
            arr[x][y].background = '#d36a6a'
          } else {
            arr[x + 1][y] = {name: '.', color}
            setChessBoard(arr)
          }
        }
      } else if (name === '.' && moveOrder === 'black') {
        if (x === 2) {
          arr[x - 1][y] = {x, y, name: '', color, background: ''}
          arr[x + 1][y] = {x, y, name: '', color, background: ''}
        } else if (x === 3) {
          arr[x - 1][y] = {x, y, name: '', color, background: ''}
          arr[x - 2][y] = {x, y, name: '', color, background: ''}
        } else {
          arr[x - 1][y] = {x, y, name: '', color, background: ''}
        }
        arr[x][y] = {x, y, name: 'пешка', color, background: '' }
        setChessBoard(arr)
        setMoveOrder('white')
      }
    }
  }

  return (
    <div style={{width: '1000px', margin: '50px auto'}}>
      <div className="App">
        {chessBoard.length && chessBoard.map((el, index) => {
          return (
            <div className="row" key={index}>
              {el.map((elem, idx) => {
                return (
                  <div id={index + '_' + idx} key={idx + '_cell'} className="cell"
                       style={{
                         width: '80px',
                         height: '80px',
                         background: elem.background ? elem.background : (index + idx) % 2 === 0 ? '#afafaf' : '#696969',
                         display: 'flex',
                         justifyContent: 'center',
                         alignItems: 'center',
                         color: elem?.color === 'black' ? 'black' : 'white'
                       }}
                       onClick={() => handleClickToFigure(index + '_' + idx, elem?.name, elem?.color)}
                  >
                    {elem?.name}
                  </div>
                )
              })}
            </div>
          )
        })}
      </div>
    </div>
  );
}

export default App;


// let board =
//   [
//     [{position: 'a8'}, {position: 'b8'}, {position: 'c8'}, {position: 'd8'}, {position: 'e8'}, {position: 'f8'}, {position: 'g8'}, {position: 'h8'}],
//     [{position: 'a7'}, {position: 'b7'}, {position: 'c7'}, {position: 'd7'}, {position: 'e7'}, {position: 'f7'}, {position: 'g7'}, {position: 'h7'}],
//     [{position: 'a6'}, {position: 'b6'}, {position: 'c6'}, {position: 'd6'}, {position: 'e6'}, {position: 'f6'}, {position: 'g6'}, {position: 'h6'}],
//     [{position: 'a5'}, {position: 'b5'}, {position: 'c5'}, {position: 'd5'}, {position: 'e5'}, {position: 'f5'}, {position: 'g5'}, {position: 'h5'}],
//     [{position: 'a4'}, {position: 'b4'}, {position: 'c4'}, {position: 'd4'}, {position: 'e4'}, {position: 'f4'}, {position: 'g4'}, {position: 'h4'}],
//     [{position: 'a3'}, {position: 'b3'}, {position: 'c3'}, {position: 'd3'}, {position: 'e3'}, {position: 'f3'}, {position: 'g3'}, {position: 'h3'}],
//     [{position: 'a2'}, {position: 'b2'}, {position: 'c2'}, {position: 'd2'}, {position: 'e2'}, {position: 'f2'}, {position: 'g2'}, {position: 'h2'}],
//     [{position: 'a1'}, {position: 'b1'}, {position: 'c1'}, {position: 'd1'}, {position: 'e1'}, {position: 'f1'}, {position: 'g1'}, {position: 'h1'}]
//   ]